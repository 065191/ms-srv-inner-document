package ru.sberbank.cseodo.ms.document.jpa;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = false, includeFieldNames = true)
public class FetchDocItemsQuery {

    private int offset;
    private int limit;
    private DocumentFilter filter;

}
