package ru.sberbank.cseodo.ms.document.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sberbank.cseodo.ms.document.jpa.DocItemEntity;

public interface DocItemEntityRepository extends JpaRepository<DocItemEntity, String> {
}
