package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

// data class CancelCmd(@TargetAggregateIdentifier val id: String)
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = false, includeFieldNames = true)
public class CancelCmd {

    @NonNull
    @TargetAggregateIdentifier
    String id;
}
