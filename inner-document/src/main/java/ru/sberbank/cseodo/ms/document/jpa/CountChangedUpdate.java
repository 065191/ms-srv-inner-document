package ru.sberbank.cseodo.ms.document.jpa;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@ToString(callSuper = false, includeFieldNames = true)
@AllArgsConstructor
public class CountChangedUpdate {
}
