package ru.sberbank.cseodo.ms.document.jpa.jpaprojection;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.DeletedItemEvt;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.EditedItemEvt;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.IssuedItemEvt;
import ru.sberbank.cseodo.ms.document.api.mappers.DocumentMapper;
import ru.sberbank.cseodo.ms.document.common.DelayedQueues;
import ru.sberbank.cseodo.ms.document.jpa.*;
import ru.sberbank.cseodo.ms.document.jpa.repository.DocItemEntityRepository;
import ru.sberbank.cseodo.ms.document.jpa.repository.DocumentSummaryEntityRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.Instant;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@Slf4j
@Component
@Profile("query")
public class DocItemsProjection {


    private final EntityManager entityManager;
    private final QueryUpdateEmitter queryUpdateEmitter;
    private final DocumentSummaryEntityRepository documentSummaryEntityRepo;
    private final DocItemEntityRepository docItemEntityRepo;
    DocumentMapper documentMapper;
    private DelayedQueues delayQueMap;

    public DocItemsProjection(EntityManager entityManager,
                              @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") QueryUpdateEmitter queryUpdateEmitter,
                              DocumentSummaryEntityRepository documentSummaryEntityRepo,
                              DocItemEntityRepository docItemEntityRepo,
                              DocumentMapper documentMapper) {
        this.entityManager = entityManager;
        this.queryUpdateEmitter = queryUpdateEmitter;
        this.documentSummaryEntityRepo = documentSummaryEntityRepo;
        this.docItemEntityRepo = docItemEntityRepo;
        this.documentMapper = documentMapper;
        this.delayQueMap = new DelayedQueues();
    }

    @EventHandler
    public void on(IssuedItemEvt event) {
        log.debug("projecting {}", event);
        /*
         * Update our read model by inserting the new card. This is done so that upcoming regular
         * (non-subscription) queries get correct data.
         */
        //DocumentSummaryEntity document = entityManager.find(DocumentSummaryEntity.class, event.getDocumentGuid());
        String docGuid = event.getDocumentGuid();

        if (!documentSummaryEntityRepo.existsById(docGuid)) {
            log.debug("projecting NotFound doc {} for item {}", docGuid, event);
            Queue<DocItemEntity> itemEntityQueue = delayQueMap
                    .getItemEntityMap()
                    .getOrDefault(docGuid, new ConcurrentLinkedQueue<DocItemEntity>());
            // documentMapper.eventItemToEntity(event);
            DocItemEntity docItemEntity = documentMapper.eventItemIssuedToEntity(event);
            //new DocItemEntity(docGuid, null, event.getItemCode(), event.getContent() );

            if (!itemEntityQueue.contains(docItemEntity)) {
                itemEntityQueue.add(docItemEntity);
            }

            delayQueMap.getItemEntityMap()
                    .put(docGuid, itemEntityQueue);
            delayQueMap.updateShedulerIdle();

            queryUpdateEmitter.emit(CountDocItemsQuery.class,
                    query -> event.getItemGuid().startsWith(query.getFilter().getIdStartsWith()),
                    new CountChangedUpdate());
            return;
        }

        DocumentSummaryEntity document = documentSummaryEntityRepo.getOne(docGuid);
        Queue<DocItemEntity> entityQueue;
        DocItemEntity itemEntity;

        // ВАЖНОЕ!!! T0D0 - обслуживание остальных не пустых очередей вывести в фоновый поток
        // он должен узнавать(по триггеру/событию/ - через аксон или многопоточность)/искать
        // о появлении не пустых очередей и периодически пробовать их исполнить
        if ((entityQueue = delayQueMap.getItemEntityMap().get(docGuid)) != null) {

            while (entityQueue.size() > 0) {
                if ((itemEntity = entityQueue.poll()) != null) {
                    itemEntity.setDocument(document);
                    log.debug("projecting Persisting {}", itemEntity);
                    //docItemEntityRepo.saveAndFlush(itemEntity);
                    //docItemEntityRepo.save(itemEntity);
                    entityManager.persist(itemEntity);
                }
            }
        }
        //DocumentSummaryEntity document = documentSummaryEntityRepo.getOne(event.getDocumentGuid());

        DocItemEntity docItemEntity = new DocItemEntity(event.getItemGuid(),
                document,
                event.getItemCode(),
                event.getContent());
        docItemEntity.setSortOrder(event.getSortOrder());
        docItemEntity.setEncrypted(event.isEncrypted());
        docItemEntity.setDocument(document);

        entityManager.persist(docItemEntity);
        //docItemEntityRepo.save(docItemEntity);
        //docItemEntityRepo.saveAndFlush(docItemEntity);

        //entityManager.find(DocumentSummaryEntity.class, event.getDocumentGuid());
        //entityManager.find(DocItemEntity.class, event.getItemGuid());
        /*
         * Serve the subscribed queries by emitting an update. This reads as follows:
         * - to all current subscriptions of type CountDocItemsQuery
         * - for which is true that the id of the doc demo having been issued starts with the idStartWith string
         *   in the query's filter
         * - send a message that the count of queries matching this query has been changed.
         */
        queryUpdateEmitter.emit(CountDocItemsQuery.class,
                query -> event.getItemGuid().startsWith(query.getFilter().getIdStartsWith()),
                new CountChangedUpdate());
        delayQueMap.setDelayIdle(1);
    }

    @EventHandler
    public void on(EditedItemEvt event) {
        log.debug("projecting {}", event);
        /*
         * Update our read model by updating the existing card. This is done so that upcoming regular
         * (non-subscription) queries get correct data.
         */
        /*
        DocumentSummaryEntity document = entityManager.find(DocumentSummaryEntity.class, event.getDocumentGuid());
        DocItemEntity docItemEntity = entityManager.find(DocItemEntity.class, event.getItemGuid());
        */

        String docGuid = event.getDocumentGuid();
        String evtGuid = event.getItemGuid();

        if (!docItemEntityRepo.existsById(evtGuid)) {
            log.debug("projecting NotFound item {} for doc {}", event, docGuid);
            Queue<DocItemEntity> itemEntityQueue = delayQueMap
                    .getItemEntityMap()
                    .getOrDefault(docGuid, new ConcurrentLinkedQueue<DocItemEntity>());
            // documentMapper.eventItemToEntity(event);
            DocItemEntity docItemEntity = documentMapper.eventItemEditedToEntity(event);
            //new DocItemEntity(docGuid, null, event.getItemCode(), event.getContent() );

            if (!itemEntityQueue.contains(docItemEntity)) {
                itemEntityQueue.add(docItemEntity);
            }

            delayQueMap.getItemEntityMap()
                    .put(docGuid, itemEntityQueue);
            delayQueMap.updateShedulerIdle();

            queryUpdateEmitter.emit(CountDocItemsQuery.class,
                    query -> event.getItemGuid().startsWith(query.getFilter().getIdStartsWith()),
                    new CountChangedUpdate());
            return;
        }

        DocumentSummaryEntity document = documentSummaryEntityRepo.getOne(event.getDocumentGuid());
        Queue<DocItemEntity> entityQueue;
        DocItemEntity itemEntity;

        // ВАЖНОЕ!!! T0D0 - обслуживание остальных не пустых очередей вывести в фоновый поток
        // он должен узнавать(по триггеру/событию/ - через аксон или многопоточность)/искать
        // о появлении не пустых очередей и периодически пробовать их исполнить
        if ((entityQueue = delayQueMap.getItemEntityMap().get(docGuid)) != null) {
            while (entityQueue.size() > 0) {
                if ((itemEntity = entityQueue.poll()) != null) {
                    itemEntity.setDocument(document);
                    log.debug("projecting delayed EditedItemEvt {}", itemEntity);
                    entityManager.persist(itemEntity);
                }
            }
        }


        DocItemEntity docItemEntity = docItemEntityRepo.getOne(event.getItemGuid());

        docItemEntity.setDocument(document);
        docItemEntity.setItemCode(event.getItemCode());
        docItemEntity.setSortOrder(event.getSortOrder());
        docItemEntity.setEncrypted(event.isEncrypted());
        docItemEntity.setDocumentContent(new DocumentContent(event.getContent()));

        /*
         * Serve the subscribed queries by emitting an update. This reads as follows:
         * - to all current subscriptions of type FetchDocSummariesQuery
         * - for which is true that the id of the doc demo having been redeemed starts with the idStartWith string
         *   in the query's filter
         * - send a message containing the new state of this doc demo summary
         */
        queryUpdateEmitter.emit(FetchDocItemsQuery.class,
                query -> event.getItemGuid().startsWith(query.getFilter().getIdStartsWith()),
                docItemEntity);
    }

    @EventHandler
    public void on(DeletedItemEvt event) {
        log.debug("projecting {}", event);

        String docGuid = event.getDocumentGuid();
        String evtGuid = event.getItemGuid();

        DocItemEntity docItemEntity = docItemEntityRepo.getOne(event.getItemGuid());

        docItemEntity.setDeleted(event.isDeleted());

        /*
         * Serve the subscribed queries by emitting an update. This reads as follows:
         * - to all current subscriptions of type FetchDocSummariesQuery
         * - for which is true that the id of the doc demo having been redeemed starts with the idStartWith string
         *   in the query's filter
         * - send a message containing the new state of this doc demo summary
         */
        queryUpdateEmitter.emit(FetchDocItemsQuery.class,
                query -> event.getItemGuid().startsWith(query.getFilter().getIdStartsWith()),
                docItemEntity);
    }



    @QueryHandler
    public List<DocItemEntity> handle(FetchDocItemsQuery query) {
        log.debug("handling {}", query);
        TypedQuery<DocItemEntity> jpaQuery =
                entityManager.createNamedQuery("DocItemEntity.fetch", DocItemEntity.class);
        jpaQuery.setParameter("idStartsWith", query.getFilter().getIdStartsWith());
        jpaQuery.setFirstResult(query.getOffset());
        jpaQuery.setMaxResults(query.getLimit());
        return jpaQuery.getResultList();
    }

    @QueryHandler
    public CountDocItemsResponse handle(CountDocItemsQuery query) {
        log.debug("handling {}", query);
        //noinspection JpaQueryApiInspection
        TypedQuery<Long> jpaQuery = entityManager.createNamedQuery("DocItemEntity.counts", Long.class);
        jpaQuery.setParameter("idStartsWith", query.getFilter().getIdStartsWith());
        return new CountDocItemsResponse(jpaQuery.getSingleResult().intValue(), Instant.now().toEpochMilli());
    }

}
