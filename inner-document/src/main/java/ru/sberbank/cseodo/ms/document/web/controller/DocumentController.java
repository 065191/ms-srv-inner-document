package ru.sberbank.cseodo.ms.document.web.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sberbank.cseodo.ms.document.api.mappers.DocumentMapper;
import ru.sberbank.cseodo.ms.document.common.dto.DocItemDto;
import ru.sberbank.cseodo.ms.document.common.dto.DocumentSummaryDto;
import ru.sberbank.cseodo.ms.document.jpa.DocumentSummaryEntity;
import ru.sberbank.cseodo.ms.document.web.service.DocumentService;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/SberbankCSEODO/CSEODO/1/document")
public class DocumentController {

    DocumentService documentService;
    DocumentMapper documentMapper;

    @GetMapping
    public ResponseEntity<DocumentSummaryDto> getDocument(@RequestParam(value = "guid") UUID documentGuid,
                                                          @RequestHeader Map<String, String> headerMap)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        DocumentSummaryEntity summaryEntity = documentService.getDocument(documentGuid);
        DocumentSummaryDto document = documentMapper.entityDocToDto(summaryEntity);

        return new ResponseEntity<>(document, HttpStatus.OK);
    }

    @PostMapping(value = "create")
    public ResponseEntity<Void> create(@RequestHeader Map<String, String> headerMap,
                                       @RequestBody DocumentSummaryDto document) {

        documentService.create(document);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "delete")
    public ResponseEntity<Void> delete(@RequestHeader Map<String, String> headerMap,
                                       @RequestBody DocumentSummaryDto document) {

        documentService.delete(document);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "item/add")
    public ResponseEntity<Void> itemAdd(@RequestHeader Map<String, String> headerMap,
                                        @RequestBody DocItemDto item) {

        documentService.itemAdd(item, Optional.empty());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "item/edit")
    public ResponseEntity<Void> itemEdit(@RequestHeader Map<String, String> headerMap,
                                        @RequestBody DocItemDto item) {

        documentService.itemEdit(item);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "item/remove")
    public ResponseEntity<Void> itemRemove(@RequestHeader Map<String, String> headerMap,
                                         @RequestBody DocItemDto item) {

        documentService.itemRemove(item);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
