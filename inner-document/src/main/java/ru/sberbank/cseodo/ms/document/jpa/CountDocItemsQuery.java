package ru.sberbank.cseodo.ms.document.jpa;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CountDocItemsQuery {

    private DocumentFilter filter;

    public CountDocItemsQuery() {
        this.filter = new DocumentFilter();
    }

    @Override
    public String toString() {
        return "CountDocItemsQuery";
    }
}
