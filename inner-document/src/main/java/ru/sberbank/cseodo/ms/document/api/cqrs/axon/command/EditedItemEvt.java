package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;


//data class EditedItemEvt(val id: String, val valueEvent: String)

import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

/**
 * Класс события обновления блока документа
 *
 * @version 1.0
 *
*/
@Data
@ToString(callSuper = false, includeFieldNames = true)
public class EditedItemEvt {
    @NonNull
    private String itemGuid;
    @NonNull
    private String documentGuid;
    @NonNull
    private String itemCode;
    // очередность расположения блока в теле документа
    private BigDecimal sortOrder;
    // идентификатор безопасности блока
    @Setter
    private boolean encrypted;
    private String content;
    @Setter
    private boolean isDeleted;

    public EditedItemEvt(String itemGuid, String documentGuid, String itemCode, String content) {

        this.itemGuid = itemGuid;
        this.documentGuid = documentGuid;
        this.itemCode = itemCode;
        this.sortOrder = new BigDecimal(0.0);
        this.encrypted = false;
        this.content = content;
        this.isDeleted = false;
    }
}
