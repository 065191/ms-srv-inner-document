package ru.sberbank.cseodo.ms.document.common.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collection;


/**
 *
 * API
 * {
 *   "documentGuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
 *   "documentType": "string",
 *   "documentKind": "string",
 *   "documentNumber": "string",
 *   "privacy": "string",
 *   "urgency": "string",
 *   "creationDate": "2020-11-25",
 *   "items": [
 *     {
 *       "documentGuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
 *       "itemGuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
 *       "itemCode": "string",
 *       "sortOrder": 0,
 *       "encrypted": true,
 *       "content": "string"
 *     }
 *   ]
 * }
 *
 */
@Data
@ToString(callSuper = false, includeFieldNames = true)
public class DocumentSummaryDto {

    private String documentGuid;
    private String documentType;
    private String documentKind;
    private String documentNumber;
    private String privacy;
    private String urgency;
    private String creationDate;
    @Setter
    @Getter
    private boolean isDeleted;

    private Collection<DocItemDto> items;
}
