package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

// data class CancelEvt(val id: String)
@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = false, includeFieldNames = true)
public class CancelEvt {
    String id;
}
