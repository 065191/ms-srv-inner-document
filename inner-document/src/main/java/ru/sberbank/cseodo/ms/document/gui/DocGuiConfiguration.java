package ru.sberbank.cseodo.ms.document.gui;

import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.sberbank.cseodo.ms.document.jpa.CountDocSummariesQuery;
import ru.sberbank.cseodo.ms.document.jpa.CountDocSummariesResponse;

@Component
@Profile("gui")
public class DocGuiConfiguration {

    @EventListener(ApplicationReadyEvent.class)
    public void helloHub(ApplicationReadyEvent event) {
        QueryGateway queryGateway = event.getApplicationContext().getBean(QueryGateway.class);
        queryGateway.query(new CountDocSummariesQuery(),
                           ResponseTypes.instanceOf(CountDocSummariesResponse.class));
    }
}
