package ru.sberbank.cseodo.ms.document.gui;

import com.vaadin.data.provider.AbstractBackEndDataProvider;
import com.vaadin.data.provider.DataChangeEvent;
import com.vaadin.data.provider.Query;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.queryhandling.SubscriptionQueryResult;
import org.springframework.context.annotation.Configuration;
import ru.sberbank.cseodo.ms.document.jpa.*;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

@Slf4j
@Configuration
public class DocItemsDataProvider extends AbstractBackEndDataProvider<DocItemEntity, Void> {

    private static final ExecutorService executorService = Executors.newCachedThreadPool();

    private final QueryGateway queryGateway;
    @Getter
    @Setter
    @NonNull
    private final DocumentFilter filter = new DocumentFilter("");
    /**
     * We need to keep track of our current subscriptions. To avoid subscriptions being modified while we are processing
     * query updates, the methods on these class are synchronized.
     */
    private SubscriptionQueryResult<List<DocItemEntity>, DocItemEntity> fetchQueryResult;
    private SubscriptionQueryResult<CountDocItemsResponse, CountChangedUpdate> countQueryResult;

    public DocItemsDataProvider(QueryGateway queryGateway) {
        this.queryGateway = queryGateway;
    }

    @Override
    @Synchronized
    protected Stream<DocItemEntity> fetchFromBackEnd(Query<DocItemEntity, Void> query) {
        /*
         * If we are already doing a query (and are subscribed to it), cancel are subscription
         * and forget about the query.
         */
        if (fetchQueryResult != null) {
            fetchQueryResult.cancel();
            fetchQueryResult = null;
        }
        FetchDocItemsQuery fetchDocItemsQuery =
                new FetchDocItemsQuery(query.getOffset(), query.getLimit(), filter);
        log.debug("submitting {}", fetchDocItemsQuery);
        /*
         * Submitting our query as a subscription query, specifying both the initially expected
         * response type (multiple CardSummaries) as wel as the expected type of the updates
         * (single DocItemEntity object). The result is a SubscriptionQueryResult which contains
         * a project reactor Mono for the initial response, and a Flux for the updates.
         */
        fetchQueryResult = queryGateway.subscriptionQuery(fetchDocItemsQuery,
                                                          ResponseTypes.multipleInstancesOf(DocItemEntity.class),
                                                          ResponseTypes.instanceOf(DocItemEntity.class));
        /*
         * Subscribing to the updates before we get the initial results.
         */
        fetchQueryResult.updates().subscribe(
                documentItem -> {
                    log.debug("processing query update for {}: {}", fetchDocItemsQuery, documentItem);
                    /* This is a Vaadin-specific call to update the UI as a result of data changes. */
                    fireEvent(new DataChangeEvent.DataRefreshEvent<>(this, documentItem));
                });
        /*
         * Returning the initial result.
         */
        return fetchQueryResult.initialResult().block().stream();
    }

    @Override
    @Synchronized
    protected int sizeInBackEnd(Query<DocItemEntity, Void> query) {
        if (countQueryResult != null) {
            countQueryResult.cancel();
            countQueryResult = null;
        }
        CountDocItemsQuery countDocItemsQuery = new CountDocItemsQuery(filter);
        log.debug("submitting {}", countDocItemsQuery);
        countQueryResult = queryGateway.subscriptionQuery(countDocItemsQuery,
                                                          ResponseTypes.instanceOf(CountDocItemsResponse.class),
                                                          ResponseTypes.instanceOf(CountChangedUpdate.class));
        /* When the count changes (new doc demos issued), the UI has to do an entirely new query (this is
         * how the Vaadin grid works). When we're bulk issuing, it doesn't make sense to do that on every single
         * issue event. Therefore, we buffer the updates for 250 milliseconds using reactor, and do the new
         * query at most once per 250ms.
         */
        countQueryResult.updates().buffer(Duration.ofMillis(250)).subscribe(
                countChanged -> {
                    log.debug("processing query update for {}: {}", countDocItemsQuery, countChanged);
                    /* This won't do, would lead to immediate new queries, looping a few times. */
                    executorService.execute(() -> fireEvent(new DataChangeEvent<>(this)));
                });
        return countQueryResult.initialResult().block().getCount();
    }

    @Synchronized
    void shutDown() {
        if (fetchQueryResult != null) {
            fetchQueryResult.cancel();
            fetchQueryResult = null;
        }
        if (countQueryResult != null) {
            countQueryResult.cancel();
            countQueryResult = null;
        }
    }
}
