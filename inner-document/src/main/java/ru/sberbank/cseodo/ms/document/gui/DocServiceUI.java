package ru.sberbank.cseodo.ms.document.gui;

import com.vaadin.annotations.Push;
import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.modelling.command.Repository;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.DocAggregate;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.DocItemAggregate;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.EditDocCmd;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.EditItemCmd;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.IssueDocCmd;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.IssueItemCmd;
import ru.sberbank.cseodo.ms.document.jpa.CountDocSummariesQuery;
import ru.sberbank.cseodo.ms.document.jpa.CountDocSummariesResponse;
import ru.sberbank.cseodo.ms.document.jpa.DocItemEntity;
import ru.sberbank.cseodo.ms.document.jpa.DocumentSummaryEntity;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Slf4j
@SpringUI
@Push
@Profile("gui")
public class DocServiceUI extends UI {

    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;

    Repository<DocAggregate> docAggregateRepository;
    Repository<DocItemAggregate> itemAggregateRepository;
    @Value("${service.gui.max.inactive.interval}")
    int uiMaxInactiveInterval;
    private DocSummaryDataProvider docSummaryDataProvider;
    private DocItemsDataProvider docItemsDataProvider;
    private ScheduledFuture<?> updaterThread;

    public DocServiceUI(CommandGateway commandGateway,
                        QueryGateway queryGateway,
                        Repository<DocAggregate> docAggregateRepository,
                        Repository<DocItemAggregate> itemAggregateRepository) {
        this.commandGateway = commandGateway;
        this.queryGateway = queryGateway;
        this.docAggregateRepository = docAggregateRepository;
        this.itemAggregateRepository = itemAggregateRepository;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        HorizontalLayout commandBar = new HorizontalLayout();
        commandBar.setWidth("100%");
        commandBar.addComponents(issuePanel(), redeemPanel(),
                                issueItemPanel(), redeemItemPanel());

        Grid summary = summaryGrid();
        Grid itemsGrid = itemsGrid();

        HorizontalLayout statusBar = new HorizontalLayout();
        Label statusLabel = new Label("Status");
        statusBar.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
        statusBar.addComponent(statusLabel);
        statusBar.setWidth("100%");

        VerticalLayout layout = new VerticalLayout();
        layout.addComponents(commandBar, summary, itemsGrid, statusBar);
        layout.setExpandRatio(summary, 1f);
        layout.setExpandRatio(itemsGrid, 1f);
        layout.setSizeFull();

        setContent(layout);

        UI.getCurrent().setErrorHandler(new DefaultErrorHandler() {
            @Override
            public void error(com.vaadin.server.ErrorEvent event) {
                Throwable cause = event.getThrowable();
                log.error("an error occured", cause);
                while (cause.getCause() != null) {
                    cause = cause.getCause();
                }
                Notification.show("Error", cause.getMessage(), Notification.Type.ERROR_MESSAGE);
            }
        });

        setPollInterval(500);
        int offset = Page.getCurrent().getWebBrowser().getTimezoneOffset();
        // offset is in milliseconds
        ZoneOffset instantOffset = ZoneOffset.ofTotalSeconds(offset / 500);
        StatusUpdater statusUpdater = new StatusUpdater(statusLabel, instantOffset);
        updaterThread = Executors.newScheduledThreadPool(1).scheduleAtFixedRate(statusUpdater, 500,
                                                                                5000, TimeUnit.MILLISECONDS);
        setPollInterval(1000);
        getSession().getSession().setMaxInactiveInterval(uiMaxInactiveInterval);
        addDetachListener((DetachListener) detachEvent -> {
            log.warn("Closing UI");
            updaterThread.cancel(true);
        });
/*
        AxonDBConfiguration axonDbConfiguration = AxonDBConfiguration
                .newBuilder("localhost:8123")
                .build();
        AxonDBEventStore axonDbEventStore = new AxonDBEventStore
                (axonDbConfiguration, XStreamSerializer.defaultSerializer());
        Configuration config = DefaultConfigurer.defaultConfiguration()
                .configureEventBus(c -> axonDbEventStore)
                // more configuration for Axon
                .buildConfiguration();
        config.start();
        //axonDbEventStore.
*/
    }

    private Panel issuePanel() {
        TextField documentGuid = new TextField("ID документа");
        TextField documentNumber = new TextField("Номер документа");
        Button submit = new Button("Создать");

        submit.addClickListener(evt -> {

            IssueDocCmd issueDocCmd = new IssueDocCmd(documentGuid.getValue());
            issueDocCmd.setDocumentNumber(documentNumber.getValue());
            issueDocCmd.setPrivacy("архисекретно");
            issueDocCmd.setUrgency("наибыстрейше");
            commandGateway.sendAndWait(issueDocCmd);
            Notification.show("Успешно", Notification.Type.HUMANIZED_MESSAGE)
                        .addCloseListener(e -> docSummaryDataProvider.refreshAll());
        });

        FormLayout form = new FormLayout();
        form.addComponents(documentGuid, documentNumber, submit);
        form.setMargin(true);

        Panel panel = new Panel("Создание документа");
        panel.setContent(form);
        return panel;
    }

    private Panel redeemPanel() {
        TextField documentGuid = new TextField("ID документа");
        TextField documentType = new TextField("Тип документа");
        TextField documentKind = new TextField("Обращение");
        TextField documentNumber = new TextField("Номер документа");
        TextField privacy = new TextField("Конфиденциальность");
        TextField urgency = new TextField("Срочность");
        TextField docCreationDate = new TextField("Дата создания");

        Button submit = new Button("Выполнить");

        submit.addClickListener(evt -> {
            EditDocCmd editDocCmd = new EditDocCmd(documentGuid.getValue());
            editDocCmd.setDocumentType(documentType.getValue());
            editDocCmd.setDocumentKind(documentKind.getValue());
            editDocCmd.setDocumentNumber(documentNumber.getValue());
            editDocCmd.setPrivacy(privacy.getValue());
            editDocCmd.setUrgency(urgency.getValue());
            editDocCmd.setCreationDate(docCreationDate.getValue());
            commandGateway.sendAndWait(editDocCmd);
            Notification.show("Успешно", Notification.Type.HUMANIZED_MESSAGE)
                        .addCloseListener(e -> docSummaryDataProvider.refreshAll());
        });

        FormLayout form = new FormLayout();
        form.addComponents(documentGuid,
                        documentType,
                        documentKind,
                        documentNumber,
                        privacy,
                        urgency,
                        docCreationDate,
                        submit);
        form.setMargin(true);

        Panel panel = new Panel("Изменение документа");
        panel.setContent(form);
        return panel;
    }

    private Panel issueItemPanel() {

        TextField itemGuid = new TextField("ID блока");
        TextField documentGuid = new TextField("ID документа");
        TextField itemCode = new TextField("Код");
        TextField sortOrder = new TextField("Порядок сортировки");
        CheckBox encrypted = new CheckBox("Зашифрован", false);
        TextField content = new TextField("Содержимое");
        Button submit = new Button("Создать");

        submit.addClickListener(evt -> {
            IssueItemCmd issueItemCmd =
                    new IssueItemCmd(itemGuid.getValue(), documentGuid.getValue(), itemCode.getValue(), content.getValue());
            issueItemCmd.setEncrypted(encrypted.getValue());
            BigDecimal sortValue = new BigDecimal(sortOrder.getValue());
            issueItemCmd.setSortOrder(sortValue);

            commandGateway.sendAndWait(issueItemCmd);
            Notification.show("Успешно", Notification.Type.HUMANIZED_MESSAGE)
                    .addCloseListener(e -> docItemsDataProvider.refreshAll());
        });

        FormLayout form = new FormLayout();
        form.addComponents(itemGuid, documentGuid, itemCode, sortOrder, encrypted, content, submit);
        form.setMargin(true);

        Panel panel = new Panel("Создание блока документа");
        panel.setContent(form);
        return panel;
    }

    private Panel redeemItemPanel() {
        TextField itemGuid = new TextField("ID блока");
        TextField documentGuid = new TextField("ID документа");
        TextField itemCode = new TextField("Код");
        TextField sortOrder = new TextField("Порядок сортировки");
        CheckBox encrypted = new CheckBox("Зашифрован", false);
        TextField content = new TextField("Содержимое");
        Button submit = new Button("Выполнить");

        submit.addClickListener(evt -> {
            EditItemCmd editItemCmd =
                    new EditItemCmd(itemGuid.getValue(), documentGuid.getValue(), itemCode.getValue(), content.getValue());
            editItemCmd.setEncrypted(encrypted.getValue());
            BigDecimal sortValue = new BigDecimal(sortOrder.getValue());
            editItemCmd.setSortOrder( sortValue );
            commandGateway.sendAndWait(editItemCmd);
            Notification.show("Успешно", Notification.Type.HUMANIZED_MESSAGE)
                    .addCloseListener(e -> docItemsDataProvider.refreshAll());
        });

        FormLayout form = new FormLayout();
        form.addComponents(itemGuid, documentGuid, itemCode, sortOrder, encrypted, content, submit);
        form.setMargin(true);

        Panel panel = new Panel("Изменение блока документа");
        panel.setContent(form);
        return panel;
    }

    private Grid summaryGrid() {
        docSummaryDataProvider = new DocSummaryDataProvider(queryGateway);
        Grid<DocumentSummaryEntity> grid = new Grid<>();
        grid.addColumn(DocumentSummaryEntity::getDocumentGuid).setCaption("ID документа");
        grid.addColumn(DocumentSummaryEntity::getDocumentType).setCaption("Тип документа");
        grid.addColumn(DocumentSummaryEntity::getDocumentKind).setCaption("Обращение");
        grid.addColumn(DocumentSummaryEntity::getDocumentNumber).setCaption("Номер документа");
        grid.addColumn(DocumentSummaryEntity::getPrivacy).setCaption("Конфиденциальность");
        grid.addColumn(DocumentSummaryEntity::getUrgency).setCaption("Срочность");
        grid.addColumn(DocumentSummaryEntity::getCreationDate).setCaption("Дата создания");
        grid.setSizeFull();
        grid.setDataProvider(docSummaryDataProvider);
        return grid;
    }

    private Grid itemsGrid() {
        docItemsDataProvider = new DocItemsDataProvider(queryGateway);
        Grid<DocItemEntity> grid = new Grid<>();
        grid.addColumn(DocItemEntity::getItemGuid).setCaption("ID блока");
        grid.addColumn(DocItemEntity::getDocument).setCaption("Документ");
        grid.addColumn(DocItemEntity::getItemCode).setCaption("Код");
        grid.addColumn(DocItemEntity::getSortOrder).setCaption("Порядок сортировки");
        grid.addColumn(DocItemEntity::getDocumentContent).setCaption("Содержимое");
        grid.addColumn(DocItemEntity::isEncrypted).setCaption("Зашифрован");
        grid.setSizeFull();
        grid.setDataProvider(docItemsDataProvider);
        return grid;
    }

    public class StatusUpdater implements Runnable {

        private final Label statusLabel;
        private final ZoneOffset instantOffset;

        public StatusUpdater(Label statusLabel, ZoneOffset instantOffset) {
            this.statusLabel = statusLabel;
            this.instantOffset = instantOffset;
        }

        @Override
        public void run() {
            CountDocSummariesQuery query = new CountDocSummariesQuery();
            queryGateway.query(
                    query, CountDocSummariesResponse.class).whenComplete((r, exception) -> {
                if (exception == null) {
                    statusLabel.setValue(Instant.ofEpochMilli(r.getLastEvent())
                                                .atOffset(instantOffset).toString());
                }
            });
        }
    }
}
