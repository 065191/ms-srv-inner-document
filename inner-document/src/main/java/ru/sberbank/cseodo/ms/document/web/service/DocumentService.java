package ru.sberbank.cseodo.ms.document.web.service;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.modelling.command.Repository;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.DocAggregate;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.DocItemAggregate;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.*;
import ru.sberbank.cseodo.ms.document.api.mappers.DocumentMapper;
import ru.sberbank.cseodo.ms.document.common.DelayedQueues;
import ru.sberbank.cseodo.ms.document.common.RestGetRetryer;
import ru.sberbank.cseodo.ms.document.common.dto.DocItemDto;
import ru.sberbank.cseodo.ms.document.common.dto.DocumentSummaryDto;
import ru.sberbank.cseodo.ms.document.gui.DocItemsDataProvider;
import ru.sberbank.cseodo.ms.document.gui.DocSummaryDataProvider;
import ru.sberbank.cseodo.ms.document.jpa.DocItemEntity;
import ru.sberbank.cseodo.ms.document.jpa.DocumentSummaryEntity;
import ru.sberbank.cseodo.ms.document.jpa.repository.DocItemEntityRepository;
import ru.sberbank.cseodo.ms.document.jpa.repository.DocumentSummaryEntityRepository;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@EnableScheduling
@Service
public class DocumentService {

    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;
    private final DocumentSummaryEntityRepository documentSummaryEntityRepo;
    private final DocItemEntityRepository docItemEntityRepo;
    private final DocSummaryDataProvider docSummaryDataProvider;
    private final DocItemsDataProvider docItemsDataProvider;
    private volatile DelayedQueues delayQueMap;
    DocumentMapper documentMapper;
    private Repository<DocAggregate> docAggregateRepository;
    private Repository<DocItemAggregate> itemAggregateRepository;

    public DocumentService(CommandGateway commandGateway,
                           QueryGateway queryGateway,
                           DocumentSummaryEntityRepository documentSummaryEntityRepo,
                           DocItemEntityRepository docItemEntityRepo,
                           DocSummaryDataProvider docSummaryDataProvider,
                           DocItemsDataProvider docItemsDataProvider,
                           Repository<DocAggregate> docAggregateRepository,
                           Repository<DocItemAggregate> itemAggregateRepository,
                           DocumentMapper documentMapper) {
        this.commandGateway = commandGateway;
        this.queryGateway = queryGateway;
        this.documentSummaryEntityRepo = documentSummaryEntityRepo;
        this.docItemEntityRepo = docItemEntityRepo;
        this.docSummaryDataProvider = docSummaryDataProvider;
        this.docItemsDataProvider = docItemsDataProvider;
        this.docAggregateRepository = docAggregateRepository;
        this.itemAggregateRepository = itemAggregateRepository;
        this.documentMapper = documentMapper;
        this.delayQueMap = new DelayedQueues();
    }

    //@Synchronized
    public DocumentSummaryEntity getDocument(UUID documentGuid) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        String docGuid = documentGuid.toString();
        //commandGateway.sendAndWait(getDocCmd);

        /*AtomicReference<Object> docAtomic = new AtomicReference(new DocumentSummaryEntity());
        AtomicReference<JpaRepository> repoAtomic = new AtomicReference(documentSummaryEntityRepo);

        Thread retryerJpaDoc = new Thread(new RestGetRetryer(docGuid, docAtomic, repoAtomic));
        retryerJpaDoc.start();
        DocumentSummaryEntity summaryEntity;
        while ((summaryEntity = (DocumentSummaryEntity) docAtomic.get()).getDocumentGuid().isEmpty()) {
            log.debug("retry of jpa doc# {} ", docGuid);
        }
        return ( !summaryEntity.getDocumentGuid().isEmpty() )
                ? summaryEntity : documentSummaryEntityRepo.getOne(docGuid);*/

        return (DocumentSummaryEntity)getEntity(new DocumentSummaryEntity(),
                                                documentSummaryEntityRepo,
                                                docGuid,
                                                "getOne");
        //return summaryEntity; -- если будем возвращать по get пустой объект, а не 500ю ошибку
    }


    // T0O0 обследовать поведение Synchronized/sendAndWait/send, в JPA должна проставляться у item связь с документом.
    // + пробросить через маппер
    // Временное решение до ответа от Axon. Items отправляем через команды ПОСЛЕ создании документа
    // и получения сообщения от родительского агрегата
    //@Synchronized
    public void create(DocumentSummaryDto document) {

        String docGuid = document.getDocumentGuid();
        log.info("create doc# {} items count = {}", docGuid, document.getItems() == null ? "0" : document.getItems().size() );

        IssueDocCmd issueDocCmd = documentMapper.docDtoToIssueCmd(document);

        if (document.getItems() != null) {
            Queue<DocItemDto> itemDtoQueue = delayQueMap.getItemsCommandsMap()
                    .getOrDefault(docGuid, new ConcurrentLinkedQueue<DocItemDto>());
            document.getItems().stream().forEach(item -> {
                itemDtoQueue.add(item);
                //log.info("create que# {} item# {}", docGuid, item.getItemGuid() );
                //itemAdd(item, Optional.ofNullable(docGuid));
            });
            delayQueMap.getItemsCommandsMap()
                    .put(docGuid, itemDtoQueue);
        }

        commandGateway.sendAndWait(issueDocCmd);
        docSummaryDataProvider.refreshAll();
    }

    // T0O0 обследовать поведение Synchronized/sendAndWait/send, в JPA должна проставляться у item связь с документом.
    // + пробросить через маппер
    @Synchronized
    public void itemAdd(DocItemDto item, Optional<String> documentGuid) {
        //log.info("serviced Add Item doc# {}, item# ", documentGuid.get(), item.getItemGuid() );

        IssueItemCmd issueItemCmd = documentMapper.itemDtoToIssueCmd(item);
        issueItemCmd.setDocumentGuid(documentGuid.isPresent() ? documentGuid.get() : item.getDocumentGuid());

        commandGateway.sendAndWait(issueItemCmd);

        docSummaryDataProvider.refreshAll();
        docItemsDataProvider.refreshAll();
    }

    // T0O0 обследовать поведение Synchronized/sendAndWait/send, в JPA должна проставляться у item связь с документом.
    //  + Продумать оптимизацию и согласовать с аналитиками стратегию обногвления,
    // такк чтобы не обновлять сущность целиком,  а только по поолям изменеий.
    // (возможно рефлекшн/разбиение на команды/события по атрибутнеому составу сущности)
    @Synchronized
    public void itemEdit(DocItemDto item) {

        //DocItemEntity docItemEntity = docItemEntityRepo.getOne(item.getItemGuid());
        EditItemCmd editItemCmd = new EditItemCmd();
        editItemCmd = documentMapper.itemDtoToEditCmd(item);

        commandGateway.sendAndWait(editItemCmd);

        docSummaryDataProvider.refreshAll();
        docItemsDataProvider.refreshAll();
    }

    public void delete(DocumentSummaryDto document) {
        String docGuid = document.getDocumentGuid();
        log.info("delete doc# {} ", docGuid );

        DeleteDocCmd deleteDocCmd = documentMapper.docDtoToDeleteCmd(document);
        deleteDocCmd.setDeleted(true);

        commandGateway.sendAndWait(deleteDocCmd);
        docSummaryDataProvider.refreshAll();
    }

    public void itemRemove(DocItemDto item) {
        String docGuid = item.getDocumentGuid();
        String itemGuid = item.getItemGuid();
        log.info("delete Item# {}, doc# {} ", itemGuid, docGuid );

        DeleteItemCmd deleteItemCmd = documentMapper.itemDtoToDeleteCmd(item);
        deleteItemCmd.setDeleted(true);

        commandGateway.sendAndWait(deleteItemCmd);
        docSummaryDataProvider.refreshAll();
    }

    @EventHandler
    public void on(IssuedDocEvt event) {
        log.debug("serviced {}", event);
        //log.debug("serviced on doc {} que size = {}", event.getDocumentGuid(), delayQueMap.getItemsCommandsMap().get(event.getDocumentGuid()).size());

        DocItemDto item;
        Queue<DocItemDto> itemDtoQueue;

        if ((itemDtoQueue = delayQueMap.getItemsCommandsMap().get(event.getDocumentGuid())) != null) {
            while (itemDtoQueue.size() > 0) {
                if ((item = itemDtoQueue.poll()) != null)
                    itemAdd(item, Optional.ofNullable(event.getDocumentGuid()));
            }
        }
    }

    @EventHandler
    public void on(DeletedDocEvt event) {
        log.debug("serviced {}", event);
        //log.debug("serviced on doc {} que size = {}", event.getDocumentGuid(), delayQueMap.getItemsCommandsMap().get(event.getDocumentGuid()).size());

        DocumentSummaryEntity summaryEntity = documentSummaryEntityRepo.getOne(event.getDocumentGuid());
        summaryEntity.getItems().stream()
                .filter(f -> !f.isDeleted() )
                .forEach(item -> {
                    DeleteItemCmd deleteItemCmd = documentMapper.itemEntityToDeleteCmd(item);
                    deleteItemCmd.setDeleted(true);
                    commandGateway.sendAndWait(deleteItemCmd);
                });


        docSummaryDataProvider.refreshAll();
        docItemsDataProvider.refreshAll();

    }

    // Вернет либо пустую энтити, либо найденную
    Object getEntity(Object entity, JpaRepository repo, String guidEntity, String repoMethodName)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        AtomicReference<Object> docAtomic = new AtomicReference(entity);
        AtomicReference<JpaRepository> repoAtomic = new AtomicReference(repo);

        Thread retryerJpaDoc = new Thread(new RestGetRetryer(guidEntity, docAtomic, repoAtomic));
        retryerJpaDoc.start();
        Object summaryEntity;

        while ((summaryEntity = docAtomic.get()).equals(entity) && retryerJpaDoc.isAlive()) {
            log.debug("retry of jpa entity# {} ", guidEntity);
        }
        //getOne - метод получения из JpaRepository, но могут быть и др. findBy..  и т.д.
        Method method = repo.getClass().getMethod(repoMethodName, Object.class);
        method.setAccessible(true);

        //return summaryEntity;
        return method.invoke(repo, guidEntity); //-- если будем возвращать по get 500ю ошибку
    }

    @Scheduled( fixedDelayString = "${service.scheduler.millisec}" )
    public void runDelyedQues() throws InterruptedException {

        log.debug("sheduled task of jpa ItemEntity");
        Thread.currentThread().setName("runDelyedQues");
        // ВАЖНОЕ!!! T0D0 - обслуживание остальных не пустых очередей
        // он должен узнавать(по триггеру/событию/ - через аксон или многопоточность)/искать
        // появлениt не пустых очередей и периодически пробовать их исполнить
        delayQueMap.getItemEntityMap().entrySet().stream()
            .filter(mapEntry -> mapEntry.getValue().size() > 0 )
            .forEach(queMapEntry ->{
                if (documentSummaryEntityRepo.existsById(queMapEntry.getKey())){
                    DocumentSummaryEntity document = documentSummaryEntityRepo.getOne(queMapEntry.getKey());
                    DocItemEntity itemEntity;
                    while (queMapEntry.getValue().size() > 0) {
                        if ((itemEntity = queMapEntry.getValue().poll()) != null) {
                            itemEntity.setDocument(document);
                            log.debug("serviced Persisting {}", itemEntity);
                            docItemEntityRepo.save(itemEntity);
                            //entityManager.persist(itemEntity);
                        }
                    }
                }
            });
        delayQueMap.updateShedulerIdle();
        // перевести либо на управляемый триггер, либо на схему wait/notify
        try {
            Thread.sleep(delayQueMap.getDelayIdle());
        } catch (InterruptedException e) {
            log.debug("Interrupted sleep {}", e );
        }
    }
}
