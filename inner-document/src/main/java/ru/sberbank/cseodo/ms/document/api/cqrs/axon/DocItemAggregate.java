package ru.sberbank.cseodo.ms.document.api.cqrs.axon;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.context.annotation.Profile;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.*;

import javax.persistence.Column;
import java.math.BigDecimal;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Slf4j
@Aggregate(snapshotTriggerDefinition = "documentItemSnapshotTrigger")
@Profile("command")
public class DocItemAggregate {

    @AggregateIdentifier
    private String itemGuid;
    private String documentGuid;
    private String itemCode;
    private BigDecimal sortOrder;
    private boolean encrypted;
    @Column(length=4000)
    private String content;
    private boolean isDeleted = false;


    public DocItemAggregate() {
        // Required by Axon
        log.debug("Empty constructor invoked");
    }

    @CommandHandler
    public DocItemAggregate(IssueItemCmd cmd) {
        log.debug("handling {}", cmd);
        if ("".equals(cmd.getItemGuid()) || "".equals(cmd.getDocumentGuid())) {
            throw new IllegalArgumentException("requared valueComand is empty");
        }
        IssuedItemEvt issuedItemEvt =
                new IssuedItemEvt(cmd.getItemGuid(), cmd.getDocumentGuid(), cmd.getItemCode(), cmd.getContent());
        issuedItemEvt.setEncrypted(cmd.isEncrypted());
        issuedItemEvt.setSortOrder(cmd.getSortOrder());
        apply(issuedItemEvt);
    }

    @CommandHandler
    public void handle(EditItemCmd cmd) {
        log.debug("handling {}", cmd);
        if ("".equals(cmd.getDocumentGuid())) {
            throw new IllegalArgumentException("requared valueEvent is empty");
        }
        EditedItemEvt editedItemEvt =
                new EditedItemEvt(this.itemGuid, cmd.getDocumentGuid(), cmd.getItemCode(), cmd.getContent());
        editedItemEvt.setEncrypted(cmd.isEncrypted());
        editedItemEvt.setSortOrder(cmd.getSortOrder());
        editedItemEvt.setDeleted(cmd.isDeleted());
        apply(editedItemEvt);
    }

    @CommandHandler
    public void handle(DeleteItemCmd cmd) {
        log.debug("handling {}", cmd);
        if ("".equals(cmd.getItemGuid())) {
            throw new IllegalArgumentException("requared valueEvent is empty");
        }
        DeletedItemEvt deletedItemEvt =
                new DeletedItemEvt(this.itemGuid, cmd.getDocumentGuid(), cmd.isDeleted());
        apply(deletedItemEvt);
    }

    @CommandHandler
    public void handle(CancelCmd cmd) {
        log.debug("handling {}", cmd);
        apply(new CancelEvt(this.itemGuid));
    }

    @EventSourcingHandler
    public void on(IssuedItemEvt evt) {
        log.debug("applying {}", evt);
        itemGuid = evt.getItemGuid();
        documentGuid = evt.getDocumentGuid();
        itemCode = evt.getItemCode();
        sortOrder = evt.getSortOrder();
        encrypted = evt.isEncrypted();
        content = evt.getContent();
    }

    @EventSourcingHandler
    public void on(EditedItemEvt evt) {
        log.debug("applying {}", evt);
        documentGuid = evt.getDocumentGuid();
        itemCode = evt.getItemCode();
        sortOrder = evt.getSortOrder();
        encrypted = evt.isEncrypted();
        content = evt.getContent();
        isDeleted = evt.isDeleted();
    }

    @EventSourcingHandler
    public void on(DeletedItemEvt evt) {
        log.info("applying {}", evt);
        isDeleted = evt.isDeleted();
    }

    @EventSourcingHandler
    public void on(CancelEvt evt) {
        log.debug("applying {}", evt);
        content = "cancel item";
    }
}
