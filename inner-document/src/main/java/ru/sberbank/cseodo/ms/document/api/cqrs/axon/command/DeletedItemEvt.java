package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.NonNull;


/**
 * Класс события удаления блока в документе
 *
 * @version 1.0
 *
 */
@Data
@AllArgsConstructor
@ToString(callSuper = false, includeFieldNames = true)
public class DeletedItemEvt {

    @NonNull
    private String itemGuid;
    @NonNull
    private String documentGuid;
    @Setter
    private boolean isDeleted;
}
