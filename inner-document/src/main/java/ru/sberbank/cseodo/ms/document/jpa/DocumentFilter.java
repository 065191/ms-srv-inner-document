package ru.sberbank.cseodo.ms.document.jpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString(callSuper = false, includeFieldNames = true)
public class DocumentFilter {

    private String idStartsWith;

    public DocumentFilter(){
        idStartsWith = "";
    }

}
