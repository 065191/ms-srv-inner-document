package ru.sberbank.cseodo.ms.document.jpa.jpaprojection;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.DeletedDocEvt;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.EditedDocEvt;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.IssuedDocEvt;
import ru.sberbank.cseodo.ms.document.jpa.*;
import ru.sberbank.cseodo.ms.document.jpa.repository.DocumentSummaryEntityRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.time.Instant;
import java.util.List;

@Slf4j
@Component
@Profile("query")
public class DocSummaryProjection {


    private final EntityManager entityManager;
    private final DocumentSummaryEntityRepository documentSummaryEntityRepo;
    private final QueryUpdateEmitter queryUpdateEmitter;

/*
    private final EntityManager targetEntityManager;
    private final EventStorageEngine jpaStorageEngine;
*/
    public DocSummaryProjection(EntityManager entityManager,
                                DocumentSummaryEntityRepository documentSummaryEntityRepo,
                                @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
                                QueryUpdateEmitter queryUpdateEmitter
                                //, Serializer defaultSerializer,
                                //PersistenceExceptionResolver persistenceExceptionResolver,
                                //@Qualifier("eventSerializer") Serializer eventSerializer,
                                //@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") AxonConfiguration configuration,
                                //TransactionManager transactionManager
    ) {
        this.entityManager = entityManager;
        this.documentSummaryEntityRepo = documentSummaryEntityRepo;
        this.queryUpdateEmitter = queryUpdateEmitter;
        /*
        this.targetEntityManager = Persistence.createEntityManagerFactory("eventStoreAxon")
                .createEntityManager();

        this.jpaStorageEngine = JpaEventStorageEngine.builder()
                .snapshotSerializer(defaultSerializer)
                .upcasterChain(configuration.upcasterChain())
                .persistenceExceptionResolver(persistenceExceptionResolver)
                .eventSerializer(eventSerializer)
                .entityManagerProvider(new EntityManagerProviderWrapper(targetEntityManager))
                .transactionManager(transactionManager)
                .build();
         */
    }

    @EventHandler
    public void on(IssuedDocEvt event) {
        log.debug("projecting {}", event);
        /*
         * Update our read model by inserting the new card. This is done so that upcoming regular
         * (non-subscription) queries get correct data.
         */
        DocumentSummaryEntity documentSummaryEntity = new DocumentSummaryEntity(event.getDocumentGuid(),
                                                            event.getDocumentType(),
                                                            event.getDocumentKind());
        documentSummaryEntity.setDocumentNumber(event.getDocumentNumber());
        documentSummaryEntity.setPrivacy(event.getPrivacy());
        documentSummaryEntity.setUrgency(event.getUrgency());
        documentSummaryEntity.setCreationDate(event.getCreationDate());

        entityManager.persist(documentSummaryEntity);
        /*
         * Serve the subscribed queries by emitting an update. This reads as follows:
         * - to all current subscriptions of type CountDocSummariesQuery
         * - for which is true that the id of the doc demo having been issued starts with the idStartWith string
         *   in the query's filter
         * - send a message that the count of queries matching this query has been changed.
         */
        queryUpdateEmitter.emit(CountDocSummariesQuery.class,
                                query -> event.getDocumentGuid().startsWith(query.getFilter().getIdStartsWith()),
                                new CountChangedUpdate());
    }

    @EventHandler
    public void on(EditedDocEvt event) {
        log.debug("projecting {}", event);
        /*
         * Update our read model by updating the existing card. This is done so that upcoming regular
         * (non-subscription) queries get correct data.
         */
        DocumentSummaryEntity summary = entityManager.find(DocumentSummaryEntity.class, event.getDocumentGuid());
        summary.setDocumentType(event.getDocumentType());
        summary.setDocumentKind(event.getDocumentKind());
        summary.setDocumentNumber(event.getDocumentNumber());
        summary.setPrivacy(event.getPrivacy());
        summary.setUrgency(event.getUrgency());
        summary.setCreationDate(event.getCreationDate());
        /*
         * Serve the subscribed queries by emitting an update. This reads as follows:
         * - to all current subscriptions of type FetchDocSummariesQuery
         * - for which is true that the id of the doc demo having been redeemed starts with the idStartWith string
         *   in the query's filter
         * - send a message containing the new state of this doc demo summary
         */
        queryUpdateEmitter.emit(FetchDocSummariesQuery.class,
                                query -> event.getDocumentGuid().startsWith(query.getFilter().getIdStartsWith()),
                                summary);
    }

    @EventHandler
    public void on(DeletedDocEvt event) {
        log.debug("projecting {}", event);
        /*
         * Update our read model by updating the existing card. This is done so that upcoming regular
         * (non-subscription) queries get correct data.
         */
        DocumentSummaryEntity summary = documentSummaryEntityRepo.getOne( event.getDocumentGuid());
        summary.setDeleted(event.isDeleted());
        /*
         * Serve the subscribed queries by emitting an update. This reads as follows:
         * - to all current subscriptions of type FetchDocSummariesQuery
         * - for which is true that the id of the doc demo having been redeemed starts with the idStartWith string
         *   in the query's filter
         * - send a message containing the new state of this doc demo summary
         */
        queryUpdateEmitter.emit(FetchDocSummariesQuery.class,
                query -> event.getDocumentGuid().startsWith(query.getFilter().getIdStartsWith()),
                summary);
    }

    @QueryHandler
    public List<DocumentSummaryEntity> handle(FetchDocSummariesQuery query) {
        log.debug("handling {}", query);
        TypedQuery<DocumentSummaryEntity> jpaQuery =
                entityManager.createNamedQuery("DocumentSummaryEntity.fetch", DocumentSummaryEntity.class);
        jpaQuery.setParameter("idStartsWith", query.getFilter().getIdStartsWith());
        jpaQuery.setFirstResult(query.getOffset());
        jpaQuery.setMaxResults(query.getLimit());
        return jpaQuery.getResultList();
    }

    @QueryHandler
    public CountDocSummariesResponse handle(CountDocSummariesQuery query) {
        log.debug("handling {}", query);
        //noinspection JpaQueryApiInspection
        TypedQuery<Long> jpaQuery = entityManager.createNamedQuery("DocumentSummaryEntity.counts", Long.class);
        jpaQuery.setParameter("idStartsWith", query.getFilter().getIdStartsWith());
        return new CountDocSummariesResponse(jpaQuery.getSingleResult().intValue(), Instant.now().toEpochMilli());
    }

}
