package ru.sberbank.cseodo.ms.document.common;

import lombok.AllArgsConstructor;
import org.axonframework.common.jpa.EntityManagerProvider;

import javax.persistence.EntityManager;

@AllArgsConstructor
public class EntityManagerProviderWrapper implements EntityManagerProvider {

    private final EntityManager entityManager;

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
}
