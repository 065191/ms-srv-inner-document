package ru.sberbank.cseodo.ms.document.api.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.context.annotation.Configuration;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.*;
import ru.sberbank.cseodo.ms.document.common.dto.DocItemDto;
import ru.sberbank.cseodo.ms.document.common.dto.DocumentSummaryDto;
import ru.sberbank.cseodo.ms.document.jpa.DocItemEntity;
import ru.sberbank.cseodo.ms.document.jpa.DocumentSummaryEntity;

import java.util.Collection;

@Configuration
@Mapper(componentModel="spring")
public interface DocumentMapper {

    @Mappings({
            @Mapping(target = "documentGuid", source = "document.documentGuid"),
            @Mapping(target = "content", source = "documentContent.content")
    })
    DocItemDto itemEntityToDto(DocItemEntity source);

    Collection<DocItemDto> itemEntitysToDto(Collection<DocItemEntity> source);

    DocumentSummaryDto entityDocToDto(DocumentSummaryEntity source);

    EditItemCmd itemDtoToEditCmd(DocItemDto source);

    IssueDocCmd docDtoToIssueCmd(DocumentSummaryDto source);

    IssueItemCmd itemDtoToIssueCmd(DocItemDto source);

    DeleteItemCmd itemDtoToDeleteCmd(DocItemDto source);

    @Mappings({
            @Mapping(target = "isDeleted", ignore = true)
    })
    DeleteDocCmd docDtoToDeleteCmd(DocumentSummaryDto source);

    @Mappings({
            @Mapping(target = "documentGuid", source = "document.documentGuid")
    })
    DeleteItemCmd itemEntityToDeleteCmd(DocItemEntity source);
    @Mappings({
            @Mapping(target = "documentGuid", source = "document.documentGuid")
    })
    EditItemCmd itemEntityToEditCmd(DocItemEntity source);

    @Mappings({
            @Mapping(target = "documentContent.content", source = "content")
    })
    DocItemEntity eventItemIssuedToEntity(IssuedItemEvt source);

    @Mappings({
            @Mapping(target = "documentContent.content", source = "content")
    })
    DocItemEntity eventItemEditedToEntity(EditedItemEvt source);

}
