package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;
import org.springframework.lang.NonNull;

/**
 * Класс команды удаления документа
 *
 * @version 1.0
 *
 */
@Data
@AllArgsConstructor
@ToString(callSuper = false, includeFieldNames = true)
public class DeleteDocCmd {

    @NonNull
    @TargetAggregateIdentifier
    private String documentGuid;
    @Setter
    private boolean isDeleted;
}
