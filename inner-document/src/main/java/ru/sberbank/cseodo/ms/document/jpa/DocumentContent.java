package ru.sberbank.cseodo.ms.document.jpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = false, includeFieldNames = true)
public class DocumentContent implements Serializable {
    private String content;
}
