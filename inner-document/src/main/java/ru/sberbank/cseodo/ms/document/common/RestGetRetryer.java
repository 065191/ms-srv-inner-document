package ru.sberbank.cseodo.ms.document.common;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
//@Data
@NoArgsConstructor
public class RestGetRetryer implements Runnable {

    @Value("${service.get.sleep.millisec}")
    int sleepDelay;

    @Value("${service.retry.delay.sec}")
    int retryDelay;

    private String docGuid;
    private AtomicReference<Object> entityAtomic;
    private AtomicReference<JpaRepository> repoAtomic;

    public RestGetRetryer(String docGuid,
                          AtomicReference<Object> entityAtomic,
                          AtomicReference<JpaRepository> repoAtomic) {

        this.docGuid = docGuid;
        this.entityAtomic = entityAtomic;
        this.repoAtomic = repoAtomic;
    }

    public void run() {

        try {
            LocalDateTime localDateTime = LocalDateTime.now();
            boolean isExistDoc = false;
            while (!(isExistDoc = repoAtomic.get().existsById(docGuid))) {
                log.debug("waiting of jpa doc# {} ", docGuid);
                if (isAfter(localDateTime, retryDelay)) {
                    break;
                }
                Thread.sleep(sleepDelay);
            }
            if (isExistDoc) {
                entityAtomic.set(repoAtomic.get().getOne(docGuid));
            }
        } catch (InterruptedException ex) {
            log.error("Interrupted", ex);
        }
    }

    boolean isAfter(LocalDateTime localDateTime, int retryDelay) {
        return LocalDateTime.now().isAfter(localDateTime.plusSeconds(retryDelay));
    }
}
