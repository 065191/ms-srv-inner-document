package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;

import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.NonNull;

/**
 * Класс события обновления документа
 *
 * @version 1.0
 *
 *
 *     // ($full-date) 4-2-2 date-fullyear "-" date-month "-" date-mda
 *     // https://tools.ietf.org/html/rfc3339#section-5.6
 * creationDate;
 */
@Data
@ToString(callSuper = false, includeFieldNames = true)
public class EditedDocEvt {

    @NonNull
    private String documentGuid;
    private String documentType;
    private String documentKind;
    private String documentNumber;
    private String privacy;
    private String urgency;
    private String creationDate;
    @Setter
    private boolean isDeleted;

    public EditedDocEvt(String documentGuid) {
        this.documentGuid = documentGuid;
    }
}
