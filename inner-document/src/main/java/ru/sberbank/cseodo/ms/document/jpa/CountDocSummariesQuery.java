package ru.sberbank.cseodo.ms.document.jpa;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CountDocSummariesQuery {

    private DocumentFilter filter;

    public CountDocSummariesQuery() {
        this.filter = new DocumentFilter();
    }

    @Override
    public String toString() {
        return "CountDocSummariesQuery";
    }
}
