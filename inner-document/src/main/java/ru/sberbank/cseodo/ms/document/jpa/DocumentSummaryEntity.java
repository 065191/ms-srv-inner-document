package ru.sberbank.cseodo.ms.document.jpa;

import lombok.Data;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;


/**
 * Класс документа
 *
 * @version 1.0
 *
 * ($full-date) 4-2-2 date-fullyear "-" date-month "-" date-mda
 * https://tools.ietf.org/html/rfc3339#section-5.6
 * creationDate;
 */
@Entity
//@MappedSuperclass
@Data
@NamedQuery(name = "DocumentSummaryEntity.fetch",
        query = "SELECT c FROM DocumentSummaryEntity c " +
                "WHERE c.documentGuid LIKE CONCAT(:idStartsWith, '%') AND c.isDeleted = false " +
                "ORDER BY c.documentGuid")
@NamedQuery(name = "DocumentSummaryEntity.counts",
        query = "SELECT COUNT(c) FROM DocumentSummaryEntity c WHERE c.documentGuid LIKE CONCAT(:idStartsWith, '%') " +
                "AND  c.isDeleted = false ")
public class DocumentSummaryEntity {

    @Id
    private String documentGuid;
    private String documentType;
    private String documentKind;
    private String documentNumber;
    private String privacy;
    private String urgency;
    private String creationDate;
    @Setter
    @Column(columnDefinition = "boolean default false")
    private boolean isDeleted;

    @OneToMany(mappedBy = "document", fetch = FetchType.EAGER)
    private Collection<DocItemEntity> items;

    public DocumentSummaryEntity(){
        this.documentGuid = "";
        this.documentType = "";
        this.documentKind = "";
        this.documentNumber = "";
        this.privacy = "";
        this.urgency = "";
        this.creationDate = "";
        this.isDeleted = false;
    }

    public DocumentSummaryEntity(String id, String documentType, String documentKind) {
        this.documentGuid = id;
        this.documentType = documentType;
        this.documentKind = documentKind;
        this.isDeleted = false;
    }

    @Override
    public String toString() {
        return "DocumentSummaryEntity{" +
                "documentGuid='" + documentGuid + '\'' +
                ", documentType='" + documentType + '\'' +
                ", documentKind=" + documentKind + '\'' +
                ", documentNumber=" + documentNumber + '\'' +
                ", privacy=" + privacy + '\'' +
                ", urgency=" + urgency + '\'' +
                ", isDeleted=" + isDeleted + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}