package ru.sberbank.cseodo.ms.document.api.cqrs.axon;

import lombok.var;
import org.axonframework.common.caching.Cache;
import org.axonframework.common.caching.WeakReferenceCache;
import org.axonframework.common.jdbc.PersistenceExceptionResolver;
import org.axonframework.common.jpa.EntityManagerProvider;
import org.axonframework.common.transaction.TransactionManager;
import org.axonframework.eventsourcing.EventCountSnapshotTriggerDefinition;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.SnapshotTriggerDefinition;
import org.axonframework.eventsourcing.Snapshotter;
import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine;
import org.axonframework.modelling.command.Repository;
import org.axonframework.serialization.Serializer;
import org.axonframework.spring.config.AxonConfiguration;
import org.axonframework.spring.eventsourcing.SpringAggregateSnapshotterFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaVendorAdapter;

import java.util.concurrent.Executors;


@Configuration
@Profile("command")
public class CommandConfiguration {

    @Value("${service.snapshot.trigger.doc.count}")
    int snapshotTriggerCount;
    @Value("${service.snapshot.trigger.item.count}")
    int snapshotItemTriggerCount;
    @Autowired
    private JpaVendorAdapter jpaVendorAdapter;

    //@Value("${spring.datasource.driver-class-name}")
    //private String driverClassName="org.postgresql.Driver";
/*
    @Bean
    public Repository<GiftCard> giftCardRepository(EventStore eventStore, Cache cache) {
        return EventSourcingRepository.builder(GiftCard.class)
                                      .cache(cache)
                                      .eventStore(eventStore)
                                      .build();
    }
    */

    @Bean
    public Cache cache() {
        return new WeakReferenceCache();
    }

    @Bean
    @Primary
    public Repository<DocAggregate> DocumentRepository(EventStore eventStore,
                                                       SnapshotTriggerDefinition documentSnapshotTrigger) {
        return EventSourcingRepository.builder(DocAggregate.class)
                .snapshotTriggerDefinition(documentSnapshotTrigger)
                .eventStore(eventStore)
                .build();
    }

    @Bean
    @Primary
    public Repository<DocItemAggregate> DocumentItemRepository(EventStore eventStore,
                                                               SnapshotTriggerDefinition documentItemSnapshotTrigger) {
        return EventSourcingRepository.builder(DocItemAggregate.class)
                .snapshotTriggerDefinition(documentItemSnapshotTrigger)
                .eventStore(eventStore)
                .build();
    }


    @Bean
    public SpringAggregateSnapshotterFactoryBean snapshotter() {
        var springAggregateSnapshotterFactoryBean = new SpringAggregateSnapshotterFactoryBean();
        //Setting async executors
        springAggregateSnapshotterFactoryBean.setExecutor(Executors.newSingleThreadExecutor());
        return springAggregateSnapshotterFactoryBean;
    }

    @Bean("documentSnapshotTrigger")
    public SnapshotTriggerDefinition documentSnapshotTriggerDefinition(Snapshotter snapshotter) {
        return new EventCountSnapshotTriggerDefinition(snapshotter, snapshotTriggerCount);
    }

    @Bean("documentItemSnapshotTrigger")
    public SnapshotTriggerDefinition documentItemSnapshotTriggerDefinition(Snapshotter snapshotter) {
        return new EventCountSnapshotTriggerDefinition(snapshotter, snapshotItemTriggerCount);
    }

    @Bean("jpaEventStorageEngine")
    public EventStorageEngine eventStorageEngine(  Serializer defaultSerializer,
                                                   PersistenceExceptionResolver persistenceExceptionResolver,
                                                   @Qualifier("eventSerializer") Serializer eventSerializer,
                                                   AxonConfiguration configuration,
                                                   EntityManagerProvider entityManagerProvider,
                                                   TransactionManager transactionManager) {

        return JpaEventStorageEngine.builder()
                .snapshotSerializer(defaultSerializer)
                .upcasterChain(configuration.upcasterChain())
                .persistenceExceptionResolver(persistenceExceptionResolver)
                .eventSerializer(eventSerializer)
                .entityManagerProvider(entityManagerProvider)
                .transactionManager(transactionManager)
                .build();
    }
/*
    @Bean
    @Primary
    public EntityManager targetEntityManager(){
        return Persistence.createEntityManagerFactory("eventStoreAxon").createEntityManager();
    }*/
/*
    @Bean
    @Primary
    public EntityManager axonEntityManager() {
        return axonEntityManagerFactory().createEntityManager();
    }

    @Bean
    @Primary
    public EntityManagerFactory axonEntityManagerFactory() {
        return createEntityManagerFactory("eventStoreAxon");
    }

    @Bean("transactionManager")
    @Primary
    public PlatformTransactionManager axonTransactionManager() {
        return new JpaTransactionManager(axonEntityManagerFactory());
    }

    private EntityManagerFactory createEntityManagerFactory(final String persistenceUnitName) {
        final LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();

        final DriverManagerDataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/my-db", "my-db", "my-db");
        dataSource.setDriverClassName(driverClassName);
        entityManagerFactory.setDataSource(dataSource);

        entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter);
        entityManagerFactory.setPackagesToScan("ru.sberbank.cseodo.ms");
        entityManagerFactory.setPersistenceUnitName(persistenceUnitName);

        final Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL94Dialect");
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        entityManagerFactory.setJpaProperties(properties);

        entityManagerFactory.afterPropertiesSet();
        return entityManagerFactory.getObject();
    }
*/
}
