package ru.sberbank.cseodo.ms.document;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentServiceApp {

    public static void main(String[] args) {
        SpringApplication.run(DocumentServiceApp.class, args);
    }
}
