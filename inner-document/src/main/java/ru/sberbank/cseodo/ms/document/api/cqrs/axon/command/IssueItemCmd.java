package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;

import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;
import org.springframework.lang.NonNull;

import java.math.BigDecimal;

/**
 * Класс команды создания блока в документе
 *
 * @version 1.0
 *
*/
@Data
@ToString(callSuper = false, includeFieldNames = true)
public class IssueItemCmd {

    @NonNull
    @TargetAggregateIdentifier
    private String itemGuid;
    @NonNull
    private String documentGuid;
    @NonNull
    private String itemCode;
    private BigDecimal sortOrder;
    @Setter
    private boolean encrypted;
    private String content;

    public IssueItemCmd(String itemGuid, String documentGuid, String itemCode, String content) {

        this.itemGuid = itemGuid;
        this.documentGuid = documentGuid;
        this.itemCode = itemCode;
        this.sortOrder = new BigDecimal(0.0);
        this.encrypted = false;
        this.content = content;
    }
}
