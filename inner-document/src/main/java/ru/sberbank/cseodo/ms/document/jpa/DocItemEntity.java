package ru.sberbank.cseodo.ms.document.jpa;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Класс блока документа
 *
 * @version 1.0
 *
*/
@Entity
@Data
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
})
@NamedQuery(name = "DocItemEntity.fetch",
        query = "SELECT c FROM DocItemEntity c WHERE c.itemGuid LIKE CONCAT(:idStartsWith, '%') " +
                " AND c.isDeleted = false ORDER BY c.itemGuid")
@NamedQuery(name = "DocItemEntity.fetchByDocId",
        query = "SELECT c FROM DocItemEntity c WHERE c.document.documentGuid LIKE CONCAT(:idStartsWith, '%') " +
                " AND c.isDeleted = false ORDER BY c.itemGuid")
@NamedQuery(name = "DocItemEntity.counts",
        query = "SELECT COUNT(c) FROM DocItemEntity c WHERE c.itemGuid LIKE CONCAT(:idStartsWith, '%') " +
                " AND c.isDeleted = false ")
public class DocItemEntity implements Serializable {

    @Id
    private String itemGuid;
    private String itemCode;
    private BigDecimal sortOrder;

    @Setter
    @Column(columnDefinition = "boolean default false")
    private boolean encrypted;

    @Column(columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private DocumentContent documentContent;

    @EqualsAndHashCode.Exclude
    @ManyToOne(optional = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "DOC_ENTITY_ID")
    private DocumentSummaryEntity document;

    @Setter
    @Column(columnDefinition = "boolean default false")
    private boolean isDeleted;

    public DocItemEntity(){
        this.itemGuid = "";
        this.itemCode = "";
        this.sortOrder = new BigDecimal(0.0);
        this.encrypted = false;
        this.documentContent = new DocumentContent();
        this.isDeleted = false;
    }

    public DocItemEntity(String itemGuid, DocumentSummaryEntity document, String itemCode, String content) {
        this.itemGuid = itemGuid;
        this.document = document;
        this.itemCode = itemCode;
        this.documentContent = new DocumentContent(content);
        this.isDeleted = false;
    }

    @Override
    public String toString() {
        return "DocItemEntity{" +
                "itemGuid='" + itemGuid + '\'' +
                ", itemCode='" + itemCode + '\'' +
                ", sortOrder=" + sortOrder + '\'' +
                ", encrypted=" + encrypted + '\'' +
                ", isDeleted=" + isDeleted + '\'' +
                ", content=" + documentContent.getContent() +
                '}';
    }
}
