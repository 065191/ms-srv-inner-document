package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;

import lombok.Data;
import lombok.ToString;
import org.springframework.lang.NonNull;


/**
 * Класс события создания документа
 *
 * @version 1.0
 *
 */
@Data
@ToString(callSuper = false, includeFieldNames = true)
public class IssuedDocEvt {

    @NonNull
    private String documentGuid;
    private String documentType;
    private String documentKind;
    private String documentNumber;
    private String privacy;
    private String urgency;
    private String creationDate;

    public IssuedDocEvt(String documentGuid) {
        this.documentGuid = documentGuid;
    }
}
