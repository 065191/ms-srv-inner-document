package ru.sberbank.cseodo.ms.document.jpa;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = false, includeFieldNames = true)
public class CountDocItemsResponse {
    private int count;
    private long lastEvent;
}
