package ru.sberbank.cseodo.ms.document.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sberbank.cseodo.ms.document.jpa.DocumentSummaryEntity;

public interface DocumentSummaryEntityRepository extends JpaRepository<DocumentSummaryEntity, String> {
}
