package ru.sberbank.cseodo.ms.document.api.cqrs.axon;

import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.context.annotation.Profile;
import ru.sberbank.cseodo.ms.document.api.cqrs.axon.command.*;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Slf4j
@Aggregate(snapshotTriggerDefinition = "documentSnapshotTrigger")
@Profile("command")
public class DocAggregate {

    @AggregateIdentifier
    private String documentGuid;
    private String documentType;
    private String documentKind;
    private String documentNumber;
    private String privacy;
    private String urgency;
    private String creationDate;
    private boolean isDeleted = false;

    public DocAggregate() {
        // Required by Axon
        log.info("Empty constructor invoked");
    }

    @CommandHandler
    public DocAggregate(IssueDocCmd cmd) {
        log.info("handling {}", cmd.toString());
        if ("".equals(cmd.getDocumentGuid())) {
            throw new IllegalArgumentException("valueComand is empty");
        }
        IssuedDocEvt issuedDocEvt = new IssuedDocEvt(cmd.getDocumentGuid());
        issuedDocEvt.setDocumentType(cmd.getDocumentType());
        issuedDocEvt.setDocumentKind(cmd.getDocumentKind());
        issuedDocEvt.setDocumentNumber(cmd.getDocumentNumber());
        issuedDocEvt.setPrivacy(cmd.getPrivacy());
        issuedDocEvt.setUrgency(cmd.getUrgency());
        issuedDocEvt.setCreationDate(cmd.getCreationDate());

        apply(issuedDocEvt);
    }

    @CommandHandler
    public void handle(EditDocCmd cmd) {
        log.info("handling {}", cmd);
        if ("".equals(cmd.getDocumentGuid())) {
            throw new IllegalArgumentException("valueEvent is empty");
        }
        EditedDocEvt editedDocEvt = new EditedDocEvt(this.documentGuid);
        editedDocEvt.setDeleted(cmd.isDeleted());

        apply(editedDocEvt);
    }

    @CommandHandler
    public void handle(DeleteDocCmd cmd) {
        log.info("handling {}", cmd);
        if ("".equals(cmd.getDocumentGuid())) {
            throw new IllegalArgumentException("valueEvent is empty");
        }
        DeletedDocEvt deletedDocEvt = new DeletedDocEvt(this.documentGuid, cmd.isDeleted());

        apply(deletedDocEvt);
    }

    @CommandHandler
    public void handle(CancelCmd cmd) {
        log.info("handling {}", cmd);
        apply(new CancelEvt(this.documentGuid));
    }

    @EventSourcingHandler
    public void on(IssuedDocEvt evt) {
        log.info("applying {}", evt.toString());
        documentGuid = evt.getDocumentGuid();
        documentType = evt.getDocumentType();
        documentKind = evt.getDocumentKind();
        documentNumber = evt.getDocumentNumber();
        privacy = evt.getPrivacy();
        urgency = evt.getUrgency();
        creationDate = evt.getCreationDate();
    }

    @EventSourcingHandler
    public void on(EditedDocEvt evt) {
        log.info("applying {} ", evt);
        documentType = evt.getDocumentType();
        documentKind = evt.getDocumentKind();
        documentNumber = evt.getDocumentNumber();
        privacy = evt.getPrivacy();
        urgency = evt.getUrgency();
        creationDate = evt.getCreationDate();
        isDeleted = evt.isDeleted();
    }

    @EventSourcingHandler
    public void on(DeletedDocEvt evt) {
        log.info("applying {}", evt);
        isDeleted = evt.isDeleted();
    }

    @EventSourcingHandler
    public void on(CancelEvt evt) {
        log.info("applying {}", evt);
        documentType = "canceled";
    }
}
