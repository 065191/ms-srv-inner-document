package ru.sberbank.cseodo.ms.document.common.dto;

import lombok.Data;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 *  API
 *{
 *       "documentGuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
 *       "itemGuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
 *       "itemCode": "string",
 *       "sortOrder": 0,
 *       "encrypted": true,
 *       "content": "string"
 *     }
 */
@Data
@ToString(callSuper = false, includeFieldNames = true)
public class DocItemDto {

    private String itemGuid;
    private String documentGuid;
    private String itemCode;
    private BigDecimal sortOrder;
    @Setter
    private boolean encrypted;
    private String content;
    @Setter
    private boolean isDeleted;
}
