package ru.sberbank.cseodo.ms.document.api.cqrs.axon.command;

import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.axonframework.modelling.command.TargetAggregateIdentifier;
import org.springframework.lang.NonNull;

/**
 * Класс команды обновления документа
 *
 * @version 1.0
 *
 *     // ($full-date) 4-2-2 date-fullyear "-" date-month "-" date-mda
 *     // https://tools.ietf.org/html/rfc3339#section-5.6
 * creationDate;
 */
@Data
@ToString(callSuper = false, includeFieldNames = true)
public class EditDocCmd {

    @NonNull
    @TargetAggregateIdentifier
    private String documentGuid;
    private String documentType;
    private String documentKind;
    private String documentNumber;
    private String privacy;
    private String urgency;
    private String creationDate;
    @Setter
    private boolean isDeleted;

    public EditDocCmd(String documentGuid) {

        this.documentGuid = documentGuid;
    }
}
