package ru.sberbank.cseodo.ms.document.common;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import ru.sberbank.cseodo.ms.document.common.dto.DocItemDto;
import ru.sberbank.cseodo.ms.document.jpa.DocItemEntity;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Data
public class DelayedQueues {

    private static Map<String, Queue<DocItemDto>> queuesItemCommandsMap;
    private static Map<String, Queue<DocItemEntity>> queuesItemEntityMap;
    private static volatile int idlePeriod;

    static {
        queuesItemCommandsMap = new ConcurrentHashMap<String, Queue<DocItemDto>>();
        queuesItemEntityMap = new ConcurrentHashMap<String, Queue<DocItemEntity>>();
        idlePeriod = 1000;
    }

    public Map<String, Queue<DocItemDto>> getItemsCommandsMap() {
        return queuesItemCommandsMap;
    }

    public Map<String, Queue<DocItemEntity>> getItemEntityMap() {
        return queuesItemEntityMap;
    }

    public int getDelayIdle(){ return idlePeriod;}
    public void setDelayIdle(int delay){ this.idlePeriod = delay;}

    public void updateShedulerIdle(){
        log.debug("updateShedulerIdle");
        try {
            queuesItemEntityMap.entrySet().stream()
                    .filter(entry -> entry.getValue().size() > 0)
                    .findFirst().get();
            idlePeriod = 1;
            log.debug("sheduler in work mode");
            return;
        } catch (Exception e) {
            log.debug("sheduler in idle mode");
        }
        idlePeriod = 10000;
    }

}
