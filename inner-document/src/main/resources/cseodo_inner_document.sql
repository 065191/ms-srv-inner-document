--
-- PostgreSQL database dump
--
DROP SCHEMA IF EXISTS mservice CASCADE;
CREATE SCHEMA mservice;

CREATE TABLE "mservice"."association_value_entry"
(
    "id"                BIGINT       NOT NULL,
    "association_key"   VARCHAR(255) NOT NULL,
    "association_value" VARCHAR(255),
    "saga_id"           VARCHAR(255) NOT NULL,
    "saga_type"         VARCHAR(255),
    CONSTRAINT "association_value_entry_pkey" PRIMARY KEY ("id")
);

CREATE TABLE "mservice"."doc_item_entity"
(
    "item_guid"        VARCHAR(255) NOT NULL,
    "document_content" JSONB,
    "encrypted"        BOOLEAN      NOT NULL DEFAULT false,
    "item_code"        VARCHAR(255),
    "sort_order"       numeric(19, 2),
    "doc_entity_id"    VARCHAR(255),
	"is_deleted" bool NOT NULL DEFAULT false,
    CONSTRAINT "doc_item_entity_pkey" PRIMARY KEY ("item_guid")
);

CREATE TABLE "mservice"."document_summary_entity"
(
    "document_guid"   VARCHAR(255) NOT NULL,
    "creation_date"   VARCHAR(255),
    "document_kind"   VARCHAR(255),
    "document_number" VARCHAR(255),
    "document_type"   VARCHAR(255),
    "privacy"         VARCHAR(255),
    "urgency"         VARCHAR(255),
	"is_deleted" bool NOT NULL DEFAULT false,
    CONSTRAINT "document_summary_entity_pkey" PRIMARY KEY ("document_guid")
);


CREATE TABLE "mservice"."saga_entry"
(
    "saga_id"         VARCHAR(255) NOT NULL,
    "revision"        VARCHAR(255),
    "saga_type"       VARCHAR(255),
    "serialized_saga" OID,
    CONSTRAINT "saga_entry_pkey" PRIMARY KEY ("saga_id")
);

CREATE TABLE "mservice"."token_entry"
(
    "processor_name" VARCHAR(255) NOT NULL,
    "segment"        INTEGER      NOT NULL,
    "owner"          VARCHAR(255),
    "timestamp"      VARCHAR(255) NOT NULL,
    "token"          OID,
    "token_type"     VARCHAR(255),
    CONSTRAINT "token_entry_pkey" PRIMARY KEY ("processor_name", "segment")
);

ALTER TABLE "mservice"."doc_item_entity"
    ADD CONSTRAINT "fk_doc_item_entity_id" FOREIGN KEY ("doc_entity_id")
        REFERENCES "mservice"."document_summary_entity" ("document_guid")
        ON UPDATE NO ACTION ON DELETE NO ACTION;

