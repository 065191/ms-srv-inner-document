Сведения по сборкам/бранчам.
[локальный бранч разработчика  ->  сборка для стендов]

0) Стартовая (нулевая модель) МС Документ. 

Бранчи:

dev_local  ->  develop




1) Первое изменение модели ЭДО - МС Документ

Бранчи с AXON сервером:

develop_local_v1  ->  develop_v1



Бранчи с JGroups без AXON сервера:

develop_local_command  ->  develop_command

develop_local_query    ->  develop_query




2) Переход на перспективную модель ЭДО с учетом Версий + KAFKA

(прототип develop_local_v1) develop_axon_local_v2  ->  (прототип develop_v1) develop_axon_v2

3) Модель АПИ version_2 из StopLight

develop_local_SL_version_2	->	develop_SL_version_2

4) Модель АПИ version_3 https://Back-Dev-CSEODO@bitbucket.org/sber-cseodo/api.git

develop_local_SL_v3	->	develop_SL_v3



